from turtle import title
from django.contrib import admin
from home.models import Post,Category, User, Tags
from django.contrib.auth.admin import UserAdmin

# from home.models import Image

@admin.register(Post)
class Postadmin(admin.ModelAdmin):
    list_display = ['author','title',  'content']

admin.site.register(Category)

admin.site.register(User)

admin.site.register(Tags)
