from re import A
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.forms import PasswordChangeForm
from home.models import Category, Post, Tags
from .forms import EditUserProfileForm, PostForm
from django.contrib import messages
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
# from django.contrib.auth.models import User, auth
from django.contrib.auth.decorators import login_required
from .forms import Userupdate, EditUserProfileForm
from .forms import SignUpForm
from django.views.generic.edit import FormView
from django.contrib.auth import get_user_model

def index(request):
    return render(request, 'qurno/index.html')
    # data = Post.objects.all()
    # if request.user.is_anonymous:
    #     return redirect("login")
    # return render(request, 'qurno/index.html', {'data': data})

def loginUser(request):
    if request.user.is_authenticated:
        return redirect("/")
    else:
        if request.method =="POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user =authenticate(username=username,password=password)

            if user is not None:

                login(request, user)
                return redirect("/")    

            else:

                return render(request, 'login.html')
        else:
            return render(request, 'login.html')

def logoutUser(request):
    logout(request)
    return redirect("/login")

def register(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = SignUpForm()
    return render(request, 'register.html', {'form' : form})

def index2(request):
    data = Post.objects.all()
    return render(request, 'index2.html',{'data': data})

def cat(request):
    data = Category.objects.all()
    return render(request, 'base.html',{'data': data})


def about(request):
    return render(request, 'about.html')

def elements(request):
    return render(request,'elements.html')

def archive(request):
    return render(request, 'archive.html')


def contact(request):
    if request.user.is_authenticated:  
        return render(request, 'contact.html')
    else:
        return HttpResponse("plz log in")
    
@login_required
def blog(request):
    if request.method == 'POST':
        form1 = PostForm(request.POST, request.FILES)
        if form1.is_valid():
            form1.save()
            form1 = PostForm() 
            print(form1)  
            # messages.success(request, 'Your Blog Posted successfully.')   
    else:
        form1 = PostForm()

    return render(request, 'blog.html',{'form': form1})


def blog_single(request):
    data = Post.objects.all()
    return render(request, 'blog_single.html',{'data': data})

def page_404(request):
    return render(request, 'page_404.html')

def privacy(request):
    return render(request, 'privacy.html')


def author(request): 
    user = get_user_model()  
    users = user.objects.all()  
    data = Post.objects.all()
    return render(request, 'author.html',{'data': data, 'users':users})

def author_single(request): 
    return render(request, 'author_single.html')

def tags(request):
    data = Tags.objects.all()
    return render(request, 'tags.html',{'data': data})

class TagsDetailView(DetailView):
    model = Tags
    template_name = 'single_tag.html'
    context_object_name = 'tag'

    def get_context_data(self, **kwargs):
        tags = kwargs['object']
        kwargs['posts'] = Post.objects.all().filter(tags=tags)
        return super().get_context_data(**kwargs)

class AddTagView(CreateView):
    # if request.user.is_authenticated:
    model = Tags
    template_name = 'add_tags.html'
    fields = '__all__'
    success_url = '/AddTags/'


# def tag_single(request):
#     return render(request, 'tag_single.html')


def categorie_single(request):
    return render(request, 'categorie_single.html')

def contact2(request):
    return render(request, 'contact2.html')

class AddCategoryView(CreateView):
    # if request.user.is_authenticated:
    model = Category
    template_name = 'add_category.html'
    fields = '__all__'
    success_url = '/AddCategory/'

def category_single(request):
    data = Post.objects.all()
    return render(request, 'category_single.html',{'data': data})


class CategoriesDetailView(DetailView):
    model = Category
    template_name = 'home/post.html'

    def get_context_data(self, **kwargs):
        category = kwargs['object']
        kwargs['posts'] = Post.objects.all().filter(category=category)
        return super().get_context_data(**kwargs)


class CategoriesListView(ListView):
    model = Category
    template_name = 'categories.html'
    # pk_url_kwarg = 'category_name'


class Index3DetailView(DetailView):
    model = Post
    template_name = 'home/post_det.html'
    context_object_name = 'p'

class Index3ListView(ListView):
    model = Post
    ordering = ['id']
    paginate_by = 4
 
# def index3(request):
#     data = Post.objects.all()
#     tags = Tags.objects.all()
#     return render(request, 'index3.html',{'data': data, 'tags':tags})
#     # post_list.html


def index3(request):

    tags = Tags.objects.all()

    return render(request, 'index3.html',{'tags':tags})
    # post_list.html



def change_pass(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            fm = PasswordChangeForm(user=request.user, data=request.POST)
            if fm.is_valid():
                fm.save()
                return HttpResponseRedirect('/')
        else:
            fm = PasswordChangeForm(user=request.user)
        return render(request, 'change_pass.html', {'form':fm})
    else:
        return redirect("/")


# registration.html
class UpdateTemplateView(FormView):
    template_name = "registration.html"
    form_class = Userupdate
    success_url = '/index3/'

    def get_initial(self):
        user = self.request.user
        initial = {
            "first_name": user.first_name,
            "username": user.username,
            "last_name": user.last_name,
            "email": user.email,
        }
        return initial

    def form_valid(self, form):
        user = self.request.user
        form.save(user)
        # print("form ", form)
        # if form.is_valid():
        #     form.save(kwargs.get('pk'))
        # else:
        #     return self.form_invalid(form)
        return super().form_valid(form)



@login_required
def user_profile(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            fm = EditUserProfileForm(request.POST, instance=request.user)
            if fm.is_valid():
                messages.success(request,"You Updated Profile Successfully")
                fm.save()
        else:
            fm = EditUserProfileForm(instance=request.user)
        return render(request, 'profile.html', {'name': request.user, 'form': fm})
    else:
        return HttpResponseRedirect('/')


class AuthorDetailView(DetailView):
    # user = get_user_model()  
    model = get_user_model()
    template_name = 'home/author.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        author = context['object']
        context['posts'] = Post.objects.filter(author=author).all()
        post_numbers = Post.objects.filter(author=author).all().count()
        context['post_numbers'] = post_numbers
        return context