# Generated by Django 4.0.1 on 2022-03-03 13:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0012_rename_tag_name_tags_tag'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tags',
            options={'verbose_name': 'Tag', 'verbose_name_plural': 'Tags'},
        ),
        migrations.RenameField(
            model_name='tags',
            old_name='tag',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='tags',
            name='post',
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=models.ManyToManyField(to='home.Tags'),
        ),
    ]
